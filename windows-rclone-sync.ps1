$Logfile = "C:\logs\windows-rclone-sync.log"

Function LogWrite
{
   Param ([string]$logstring)
   '{0:u}: {1}' -f (Get-Date), $logstring | Out-File $Logfile -Append
}



 Function Main { 
    LogWrite("Start windows-rclone-sync")
    LogWrite("Get variables")

    $ProgressPreference = 'SilentlyContinue' # fast Invoke-WebRequest

    Set-ItemProperty -Path HKLM:\Software\Policies\Microsoft\Windows\PowerShell -Name ExecutionPolicy -Value ByPass

    $user = [System.Environment]::GetEnvironmentVariable('USER')
    $password =[System.Environment]::GetEnvironmentVariable('WEBDAV_TOKEN') 
    $localfolder = [System.Environment]::GetEnvironmentVariable('LOCAL_FOLDER')
    $url = [System.Environment]::GetEnvironmentVariable('WEBDAY_URL')
    $webdavfolder = [System.Environment]::GetEnvironmentVariable('WEBDAV_FOLDER')

    LogWrite("Install rclone")

    $urlRclone = "https://downloads.rclone.org/rclone-current-windows-amd64.zip"
    Invoke-WebRequest -Uri $urlRclone -OutFile "$env:TEMP\rclone-windows.zip"
    Expand-Archive "$env:TEMP\rclone-windows.zip" -DestinationPath "$env:TEMP\rclone-windows-unzip"
    New-Item -Path "c:\" -Name "rclone" -ItemType "directory"
    Get-ChildItem -Path "$env:TEMP\rclone-windows-unzip\*.exe" -Recurse | Move-Item -Destination "C:\rclone\rclone.exe"
    

    $ACL = Get-Acl -Path c:\rclone
    $AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("Users", "FullControl", "ContainerInherit, ObjectInherit", "None", "Allow")
    $ACL.SetAccessRule($AccessRule)
    $ACL | Set-Acl -Path c:\rclone


    Remove-Item "$env:TEMP\rclone-windows.zip"
    Remove-Item "$env:TEMP\rclone-windows-unzip" -Recurse

    LogWrite("Set rclone.conf")  

    $encrypted_password = Invoke-Expression -Command “C:\rclone\rclone.exe obscure $password”
    
    $rclone_conf = "C:\Windows\system32\config\systemprofile\AppData\Roaming\rclone\rclone.conf"


    New-Item -ItemType File -Path $rclone_conf -Force
   
    Clear-Content $rclone_conf

    Add-Content $rclone_conf "[RD]"
    Add-Content $rclone_conf "type = webdav"
    Add-Content $rclone_conf "url = $url"
    Add-Content $rclone_conf "vendor = owncloud"
    Add-Content $rclone_conf "user = $user"
    Add-Content $rclone_conf "pass = $encrypted_password" 



    LogWrite("Create rclone scheduled task")
    $args = [string]::Concat("sync $localfolder",'"RD:', $webdavfolder, '"', "--no-console --log-file c:\logs\windows-rclone-synclog.txt")
    $action = New-ScheduledTaskAction -Execute 'c:\rclone\rclone.exe' -Argument $args

    $trigger = New-ScheduledTaskTrigger -Daily -At 12am
    $task = Register-ScheduledTask -User SYSTEM -TaskName "SRC-Rclone-sync" -Trigger $trigger -Action $action
    $task.Triggers.Repetition.Interval = "PT30M" #Repeat every 30 minutes, use PT1H for every hour
    $task | Set-ScheduledTask

    LogWrite("End windows-rclone-sync") 


 }



 try {
    Main
}
catch {
    LogWrite("$_")
    Throw $_   
}  
